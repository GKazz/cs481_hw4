import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'package:flutter/animation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';


void main() {
  runApp(MaterialApp(
    home: LogoApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(15),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 4),
                  child: Text('Trakai Castle', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold,),
                  ),
                ),
                Text('One of the castles in my home country Lithuania!',
                  style: TextStyle(fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, color: Colors.blueGrey),
                ),
              ],
            ),
          ),
          FavoriteWidget(),
        ],
      ),
    );
    Color color = Theme.of(context).primaryColor;
    Color color2 = Colors.green;
    Color color3 = Colors.purple;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            child:
            FlatButton.icon(
              onPressed: (){
                Navigator.push(context,SecondPage());
              },
              icon: Icon(Icons.filter_hdr),
              color: Colors.green,
              label: Text('Camping'),
            ),
          ),
          Container(
            child:
            FlatButton.icon(
              onPressed: (){
                Navigator.push(context,ThirdPage());
              },
              icon: Icon(Icons.computer),
              color: Colors.deepPurple,
              label: Text('Gaming'),
            ),
          ),
          Container(
            child:
            FlatButton.icon(
              onPressed: (){
                Navigator.push(context,ForthPage());
              },
              icon: Icon(Icons.ac_unit),
              color: Colors.blue,
              label: Text('SnowBoarding'),
            ),
          ),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(20),
      margin: EdgeInsets.fromLTRB(16.0, 16.0, 16.0,0),
      child: Column(
        children: <Widget>[
          SizedBox(height: 6.0),
          Text(
            '     I was born in Lithuania, which is a very small country in North Eastern Europe by the Baltic Sea.'
                ' My family moved to San Diego when I was three and I grew up here ever since.'
                ' Every few years I go back to Lithuania to visit family and discover more of my birth country.'
                ' In my free time I like to do a lot of exploring and camping.'
                ' During the winter I try to go snowboard as often as I can in Big Bear while we have some snow.'
                ' When I am not outside doing something I do like to play a lot of video games on my computer.'
                ' I enjoy to tinker with things whether thats my car or my computer, I like to see how things work and know how to fix them.'
                ' I am looking forward to learning more about mobile design in this class!',
            softWrap: true,
            style: TextStyle(fontSize: 14, color: Colors.black),
          ),
        ],
      ),
    );

    Widget textBox = Container(
      color: Colors.blue,
      child: TextField(
        onChanged: (String str) {
          print(str);
        },
      ),
    );

    Widget bottomButton = Container(
      padding: const EdgeInsets.all(2),
      child: RaisedButton(
        child: Text('Send Me A Message!',
          style: TextStyle(color: Colors.white
          ),
        ),
        color: Colors.blue,
        onPressed: (){
          Text('Not Work Yet!',
            style: TextStyle(color: Colors.white
            ),
          );
        },
      ),

    );

    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title:Text('Layout Homework'),
          backgroundColor: Colors.red,
        ),
        body: ListView(
            children: [
              Image.asset(
                'images/TrakaiNew.gif',
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
              titleSection,
              buttonSection,
              textSection,
              textBox,
              bottomButton,
            ]
        ),
      ),
    );
  }
  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 2),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}
class FavoriteWidget extends StatefulWidget{
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}
class _FavoriteWidgetState extends State<FavoriteWidget>{
  bool _isFavorited = true;
  int _favoriteCount = 41;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isFavorited ? Icon(Icons.favorite): Icon(Icons.favorite_border)),
            color: Colors.red[500],
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
              child: Text('$_favoriteCount')
          ),
        ),
      ],
    );
  }
  void _toggleFavorite(){
    setState(() {
      if(_isFavorited) {
        _favoriteCount -= 1;
        _isFavorited = false;
      } else {
        _favoriteCount += 1;
        _isFavorited = true;
      }
    });
  }
}
class SecondPage extends MaterialPageRoute<Null>{
  SecondPage() : super(builder: (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('UNDER CONSTRUCTION'),
        backgroundColor: Colors.green,
      ),
      body: Container(
        padding: EdgeInsets.all(50),
        child: RaisedButton(
          onPressed: (){
            Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));
          },
          child: Text('Return Home'),
        ),
      ),
    );
  });
}
class ThirdPage extends MaterialPageRoute<Null>{
  ThirdPage() : super(builder: (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('UNDER CONSTRUCTION'),
        backgroundColor: Colors.deepPurple,
      ),
      body: Container(
        padding: EdgeInsets.all(50),
        child: RaisedButton(
          onPressed: (){
            Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));
          },
          child: Text('Return Home'),
        ),
      ),
    );
  });
}
class ForthPage extends MaterialPageRoute<Null>{
  ForthPage() : super(builder: (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('UNDER CONSTRUCTION'),
        backgroundColor: Colors.blue,
      ),
      body: Container(
        padding: EdgeInsets.all(50),
        child: RaisedButton(
          onPressed: (){
            Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));
          },
          child: Text('Return Home'),
        ),
      ),
    );
  });
}
class LogoApp extends StatefulWidget{
  _LogoAppState createState() => _LogoAppState();
}
class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin{
  Animation<double> animation;
  AnimationController controller;
  @override
  void initState(){
    super.initState();
    new Timer(const Duration(seconds: 10), nextPage);
    controller =
        AnimationController(duration:  const Duration(seconds: 3), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if(status == AnimationStatus.completed){
          controller.reverse();
        }
        if (status == AnimationStatus.dismissed){
          controller.forward();
        }
      })
      ..addStatusListener((state) => print('$state'));

    controller.forward();
  }
  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);

  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }

  void nextPage(){
    Navigator.push(context, MaterialPageRoute(builder: (_) => new MyApp()));
  }
}
class AnimatedLogo extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: .1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 800);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: SpinKitPouringHourglass(size: 100, color: Colors.white,)
        ),
      ),
    );
  }
}